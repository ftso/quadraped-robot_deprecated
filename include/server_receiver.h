#ifndef SERVER_RECEIVER_H
#define SERVER_RECEIVER_H

#include <Arduino.h>
#include <SoftwareSerial.h>

#define TIMEOUT 5000 // mS

struct CommandAndValue {
  String command;
  int value;
};

String getCommand(String request, String cmdTofind);
int getIntCommandValue(String command);
String getCommandName(String command);

#endif
