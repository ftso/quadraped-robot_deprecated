#include <Servo.h>
#include <robot.h>

void walkOneLeg(Servo servo, int targetAngle, int restAngle, int stepSize);
void liftLeg(Servo servo);
void restLeg(Servo servo);

void goToRestPos(Robot robot);
void walkOneStep(Robot robot);

void turnRightOneStep(Robot robot);
void turnLeftOneStep(Robot robot);
