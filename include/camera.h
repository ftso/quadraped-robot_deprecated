#ifndef CAMERA_H
#define CAMERA_H

#include <Arduino.h>
#include <Wire.h>
#include <ArduCAM.h>
#include <SPI.h>
#include <memorysaver.h>
#include <server_receiver.h>

// This specifically uses Arducam Mini-2MP-Plus
#if !(defined OV2640_MINI_2MP_PLUS)
#error Please select the hardware platform and camera module in the ../libraries/ArduCAM/memorysaver.h file
#endif

const int CS_PIN = 10;
// if the video is chopped or distored, try using a lower value for the buffer
// lower values will have fewer frames per second while streaming
static const size_t BUFFER_SIZE = 4096; // 4096; //2048; //1024;

//void camera_configure();
//void captureImage();

#endif
