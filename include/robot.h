#include <Arduino.h>
#include <Servo.h>

class Robot {
  public:
    Servo servo_front_right_top;
    Servo servo_front_right_bot;
    Servo servo_front_left_top;
    Servo servo_front_left_bot;
    Servo servo_back_right_top;
    Servo servo_back_right_bot;
    Servo servo_back_left_top;
    Servo servo_back_left_bot;

    const int ANGLE_rest_front_right_topShaft = 125;
    const int ANGLE_rest_front_left_topShaft = 55;
    const int ANGLE_rest_back_left_topShaft = 90;
    const int ANGLE_rest_back_right_topShaft = 90;
    const int ANGLE_rest_botShaft = 45;
    const int SWEEP_ANGLE_front = 20; // size of step
    const int SWEEP_ANGLE_back = 35; // size of step
    const int ANGLE_res = 10;

    void attach() {
      // Assign motor pins
      servo_front_right_top.attach(7);
      servo_front_right_bot.attach(6);
      servo_front_left_top.attach(2);
      servo_front_left_bot.attach(3);
      servo_back_right_top.attach(9);
      servo_back_right_bot.attach(8);
      servo_back_left_top.attach(5);
      servo_back_left_bot.attach(4);
    }

    void detach() {
      servo_front_right_top.detach();
      servo_front_right_bot.detach();
      servo_front_left_top.detach();
      servo_front_left_bot.detach();
      servo_back_right_top.detach();
      servo_back_right_bot.detach();
      servo_back_left_top.detach();
      servo_back_left_bot.detach();
    }
};
