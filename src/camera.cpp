#include <camera.h>

// ArduCAM ARDU_CAMERA(OV2640, CS_PIN);
//
// void Camera::camera_configure() {
//   uint8_t vid, pid;
//   uint8_t temp;
//
// #if defined(__SAM3X8E__)
//   Wire1.begin();
// #else
//   Wire.begin();
// #endif
//
//   // Set CS pin as output
//   pinMode(CS_PIN, OUTPUT);
//   digitalWrite(CS_PIN, HIGH);
//
//   // initialize SPI:
//   SPI.begin();
//
//   //Reset the CPLD
//   ARDU_CAMERA.write_reg(0x07, 0x80);
//   delay(100);
//   ARDU_CAMERA.write_reg(0x07, 0x00);
//   delay(100);
//
//   while (1) {
//     //Check if the ArduCAM SPI bus is OK
//     ARDU_CAMERA.write_reg(ARDUCHIP_TEST1, 0x55);
//     temp = ARDU_CAMERA.read_reg(ARDUCHIP_TEST1);
//     if (temp != 0x55) {
//       Serial.println(F("ACK CMD SPI interface Error!END"));
//       delay(1000); continue;
//     } else {
//       Serial.println(F("ACK CMD SPI interface OK.END")); break;
//     }
//   }
//
//   while (1) {
//     //Check if the camera module type is OV2640
//     ARDU_CAMERA.wrSensorReg8_8(0xff, 0x01);
//     ARDU_CAMERA.rdSensorReg8_8(OV2640_CHIPID_HIGH, &vid);
//     ARDU_CAMERA.rdSensorReg8_8(OV2640_CHIPID_LOW, &pid);
//     if ((vid != 0x26 ) && (( pid != 0x41 ) || ( pid != 0x42 ))) {
//       Serial.println(F("ACK CMD Can't find OV2640 module!"));
//       delay(1000); continue;
//     }
//     else {
//       Serial.println(F("ACK CMD OV2640 detected.END")); break;
//     }
//   }
//
//   ARDU_CAMERA.set_format(JPEG);
//   ARDU_CAMERA.InitCAM();
//   ARDU_CAMERA.OV2640_set_JPEG_size(OV2640_320x240);
//   delay(1000);
//   ARDU_CAMERA.clear_fifo_flag();
//   delay(1000);
//
//   Serial.println("CAMERA READY");
// }
//
// void Camera::captureImage(ServerReceiver* server_receiver) {
//   ARDU_CAMERA.clear_fifo_flag();
//   ARDU_CAMERA.start_capture();
//
//   Serial.println("Capturing image");
//
//   int total_time = millis();
//    while (!ARDU_CAMERA.get_bit(ARDUCHIP_TRIG, CAP_DONE_MASK));
//    total_time = millis() - total_time;
//    Serial.print("Capture took ");
//    Serial.print(total_time, DEC);
//    Serial.println(" millis.");
//
//    size_t len = ARDU_CAMERA.read_fifo_length();
//    if (len >= 0x07ffff) {
//      Serial.print("Image over sized: ");
//      Serial.println(len, DEC);
//      return;
//    } else if (len == 0) {
//      Serial.println("Image size is 0.");
//      return;
//    }
//
//   Serial.print("Image size: ");
//   Serial.println(len, DEC);
//
//    // Set CS to LOW to prepare to receive data.
//    ARDU_CAMERA.CS_LOW();
//    ARDU_CAMERA.set_fifo_burst();
//
// #if !(defined (ARDUCAM_SHIELD_V2) && defined (OV2640_CAM))
//   SPI.transfer(0xFF);
// #endif
//
//   static uint8_t buffer[BUFFER_SIZE] = {0xFF};
//
//   String length_bits = String(len*8);
//   //server_receiver->sendCommand("AT+CIPSEND=0,"+length_bits, 3000);
//   while(len) {
//     size_t copy_size = (len < BUFFER_SIZE)? len : BUFFER_SIZE;
//     //SPI.transfer(&buffer, copy_size);
//     //server_receiver->client_send(buffer);
//     len -= copy_size;
//   }
//
//   /*uint8_t temp = 0, temp_last = 0;
//   bool is_header = false;
//   temp =  SPI.transfer(0x00);
//   len --;
//   while ( len -- )
//   {
//     temp_last = temp;
//     temp =  SPI.transfer(0x00);
//     if (is_header == true)
//     {
//       Serial.write(temp);
//     }
//     else if ((temp == 0xD8) & (temp_last == 0xFF))
//     {
//       is_header = true;
//       Serial.println(F("ACK CMD IMG END"));
//       Serial.write(temp_last);
//       Serial.write(temp);
//     }
//     if ( (temp == 0xD9) && (temp_last == 0xFF) ) { //If find the end ,break while,
//       Serial.print("Image end");
//       break;
//     }
//     delayMicroseconds(15);
//   }
//   Serial.println(imageBytes[0]);
//
//   is_header = false;*/
//
//   ARDU_CAMERA.CS_HIGH();
//   //Clear the capture done flag
//   ARDU_CAMERA.clear_fifo_flag();
// }
