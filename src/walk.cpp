#include <Arduino.h>
#include <walk.h>
#include <Servo.h>

void walkOneLeg(Servo servo, int targetAngle, int restAngle, int stepSize, Robot robot) {
  int counter = 0;
  while (counter < (stepSize/robot.ANGLE_res)) {
    counter++;

    int ANGLE_topShaft = map(counter, 1, (stepSize/robot.ANGLE_res), restAngle, targetAngle);
    servo.write(ANGLE_topShaft);
    delay(50);
  }
  delay(50);
}

void liftLeg(Servo servo, Robot robot) {
  servo.write(robot.ANGLE_rest_botShaft+30);
  delay(50);
}

void restLeg(Servo servo, Robot robot) {
  servo.write(robot.ANGLE_rest_botShaft);
  delay(50);
}

void goToRestPos(Robot robot) {
  robot.servo_front_right_top.write(robot.ANGLE_rest_front_right_topShaft);
  robot.servo_front_right_bot.write(robot.ANGLE_rest_botShaft);
  robot.servo_front_left_top.write(robot.ANGLE_rest_front_left_topShaft);
  robot.servo_front_left_bot.write(robot.ANGLE_rest_botShaft);
  robot.servo_back_right_top.write(robot.ANGLE_rest_back_right_topShaft);
  robot.servo_back_right_bot.write(robot.ANGLE_rest_botShaft);
  robot.servo_back_left_top.write(robot.ANGLE_rest_back_left_topShaft);
  robot.servo_back_left_bot.write(robot.ANGLE_rest_botShaft);
  delay(500);
}

void walkOneStep(Robot robot) {
    liftLeg(robot.servo_back_right_bot, robot);
    walkOneLeg(
      robot.servo_back_right_top,
      robot.ANGLE_rest_back_right_topShaft - robot.SWEEP_ANGLE_back,
      robot.ANGLE_rest_back_right_topShaft,
      robot.SWEEP_ANGLE_back, robot);
    restLeg(robot.servo_back_right_bot, robot);

    liftLeg(robot.servo_back_left_bot, robot);
    walkOneLeg(
      robot.servo_back_left_top,
      robot.ANGLE_rest_back_left_topShaft + robot.SWEEP_ANGLE_back,
      robot.ANGLE_rest_back_left_topShaft,
      robot.SWEEP_ANGLE_back, robot);
    restLeg(robot.servo_back_left_bot, robot);

    liftLeg(robot.servo_front_right_bot, robot);
    walkOneLeg(
      robot.servo_front_right_top,
      robot.ANGLE_rest_front_right_topShaft - robot.SWEEP_ANGLE_front,
      robot.ANGLE_rest_front_right_topShaft,
      robot.SWEEP_ANGLE_front, robot);
    restLeg(robot.servo_front_right_bot, robot);

    liftLeg(robot.servo_front_left_bot, robot);
    walkOneLeg(
      robot.servo_front_left_top,
      robot.ANGLE_rest_front_left_topShaft + robot.SWEEP_ANGLE_front,
      robot.ANGLE_rest_front_left_topShaft,
      robot.SWEEP_ANGLE_front, robot);
    restLeg(robot.servo_front_left_bot, robot);

    goToRestPos(robot);
}


void turnRightOneStep(Robot robot) {
  liftLeg(robot.servo_back_right_bot, robot);
  walkOneLeg(
    robot.servo_back_right_top,
    robot.ANGLE_rest_back_right_topShaft + robot.SWEEP_ANGLE_back,
    robot.ANGLE_rest_back_right_topShaft,
    robot.SWEEP_ANGLE_back, robot);
  restLeg(robot.servo_back_right_bot, robot);
  delay(100);

  liftLeg(robot.servo_back_left_bot, robot);
  walkOneLeg(robot.servo_back_left_top,
    robot.ANGLE_rest_back_left_topShaft + robot.SWEEP_ANGLE_back,
    robot.ANGLE_rest_back_left_topShaft,
    robot.SWEEP_ANGLE_back, robot);
  restLeg(robot.servo_back_left_bot, robot);
  delay(100);

  liftLeg(robot.servo_front_right_bot, robot);
  walkOneLeg(
    robot.servo_front_right_top,
    robot.ANGLE_rest_front_right_topShaft + robot.SWEEP_ANGLE_front,
    robot.ANGLE_rest_front_right_topShaft,
    robot.SWEEP_ANGLE_front, robot);
  restLeg(robot.servo_front_right_bot, robot);
  delay(100);

  liftLeg(robot.servo_front_left_bot, robot);
  walkOneLeg(robot.servo_front_left_top,
    robot.ANGLE_rest_front_left_topShaft + robot.SWEEP_ANGLE_front,
    robot.ANGLE_rest_front_left_topShaft,
    robot.SWEEP_ANGLE_front, robot);
  restLeg(robot.servo_front_left_bot, robot);
  delay(100);

  goToRestPos(robot);
}

void turnLeftOneStep(Robot robot) {
  liftLeg(robot.servo_back_right_bot, robot);
  walkOneLeg(
    robot.servo_back_right_top,
    robot.ANGLE_rest_back_right_topShaft - robot.SWEEP_ANGLE_back,
    robot.ANGLE_rest_back_right_topShaft,
    robot.SWEEP_ANGLE_back, robot);
  restLeg(robot.servo_back_right_bot, robot);
  delay(100);

  liftLeg(robot.servo_back_left_bot, robot);
  walkOneLeg(
    robot.servo_back_left_top,
    robot.ANGLE_rest_back_left_topShaft - robot.SWEEP_ANGLE_back,
    robot.ANGLE_rest_back_left_topShaft,
    robot.SWEEP_ANGLE_back, robot);
  restLeg(robot.servo_back_left_bot, robot);
  delay(100);

  liftLeg(robot.servo_front_right_bot, robot);
  walkOneLeg(
    robot.servo_front_right_top,
    robot.ANGLE_rest_front_right_topShaft - robot.SWEEP_ANGLE_front,
    robot.ANGLE_rest_front_right_topShaft,
    robot.SWEEP_ANGLE_front, robot);
  restLeg(robot.servo_front_right_bot, robot);
  delay(100);

  liftLeg(robot.servo_front_left_bot, robot);
  walkOneLeg(
    robot.servo_front_left_top,
    robot.ANGLE_rest_front_left_topShaft - robot.SWEEP_ANGLE_front,
    robot.ANGLE_rest_front_left_topShaft,
    robot.SWEEP_ANGLE_front, robot);
  restLeg(robot.servo_front_left_bot, robot);
  delay(100);

  goToRestPos(robot);
}
