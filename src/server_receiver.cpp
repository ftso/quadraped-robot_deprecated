#include <server_receiver.h>

String getCommand(String request, String cmdToFind) {
  int cmdStartIndex = request.indexOf(cmdToFind);
  int cmdEndIndex = request.indexOf(" ", cmdStartIndex);
  return request.substring(cmdStartIndex, cmdEndIndex);
}

int getIntCommandValue(String command) {
  int equalsIndex = command.indexOf("=");
  if (equalsIndex != -1) {
    return atoi(command.substring(equalsIndex + 1).c_str());
  } else {
    // Default value is 1.
    return 1;
  }
}

String getCommandName(String command) {
  int equalsIndex = command.indexOf("=");
  if (equalsIndex != -1) {
    return command.substring(0, equalsIndex);
  } else {
    // Default value is 1.
    return "";
  }
}
