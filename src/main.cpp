#include <Arduino.h>
#include <Servo.h>
#include <walk.h>
#include <SoftwareSerial.h>
#include <server_receiver.h>
#include <camera.h>

void server_configure();
CommandAndValue getCommandAndValue();
String sendCommand(String cmd, int timeout);
void sendResponse(String msg);
void camera_configure();

Robot robot;
ArduCAM ARDU_CAMERA(OV2640, CS_PIN);
// RX (connected to TX of ESP8266), TX (connected to RX of ESP8266)
SoftwareSerial softwareSerial(12, 11);

///////////////////////////////////////////
void setup() {
  Serial.begin(9600);
  // Configure CAMERA
  camera_configure();
  server_configure();
  delay(1000);

  // calibrate motors to rest position.
  robot.attach();
  goToRestPos(robot);
  robot.detach();

  delay(2000);
  Serial.println("EVERYTHING GO");
}

void loop() {
  CommandAndValue commandAndValue = getCommandAndValue();

  if (commandAndValue.command != "") {
    Serial.println(commandAndValue.command);
    Serial.println(commandAndValue.value);

    /*if (commandAndValue.command == "CAMERA_CAPTURE") {
      camera.captureImage(&server_receiver);
    }*/

    robot.attach();
    for (int i = 0; i < commandAndValue.value; i++) {
      if (commandAndValue.command == "FORWARD") {
        walkOneStep(robot);
      } else if (commandAndValue.command == "ROTATE_RIGHT") {
        turnRightOneStep(robot);
      } else if (commandAndValue.command == "ROTATE_LEFT") {
        turnLeftOneStep(robot);
      }
    }
    robot.detach();
    delay(1000);

    sendResponse("OK");
  }
}

/////////////////////////////////////////
//          Esp8266 Code               //
/////////////////////////////////////////
String sendCommand(String cmd, int timeout) {
  yield();
  softwareSerial.print(cmd); // Send "AT+" command to module
  Serial.println("cmd: " + cmd);

  String response = "";
  long int time = millis();
  while (time + timeout > millis()) {
    yield();
    while (softwareSerial.available()) {
      yield();
      char c = softwareSerial.read();
      response += c;
    }
  }

  Serial.println(response);
  return response;
}

void server_configure() {
  softwareSerial.begin(9600);
  delay(1000);

  sendCommand("AT+RST\r\n", 7000); // Reset the ESP8266
  sendCommand("AT+CWMODE=1\r\n",1000); // Set the ESP8266 to work as a station.
  sendCommand("AT+CWJAP=\"Burrito\",\"Melodyisthebest\"\r\n", 7000); // Setup Wifi
  sendCommand("AT+CIFSR\r\n", 2000); // Display IP address.
  sendCommand("AT+CIPMUX=1\r\n",2000); // Enable multiple connections
  sendCommand("AT+CIPSERVER=1,80\r\n",2000); // Open server mode on port 80.
}

CommandAndValue getCommandAndValue() {
  String incomingRequest = "";
  yield();

  CommandAndValue commandAndValue;

  if (softwareSerial.available()) {
   yield();

   incomingRequest = softwareSerial.readString();
   Serial.print("Received request: ");
   Serial.println(incomingRequest);
   if (incomingRequest.indexOf("FORWARD") != -1) {
     commandAndValue.command = "FORWARD";
     commandAndValue.value = getIntCommandValue(getCommand(incomingRequest, "FORWARD"));
     return commandAndValue;
   } else if (incomingRequest.indexOf("ROTATE_RIGHT") != -1) {
     commandAndValue.command = "ROTATE_RIGHT";
     commandAndValue.value = getIntCommandValue(getCommand(incomingRequest, "ROTATE_RIGHT"));
     return commandAndValue;
   } else if (incomingRequest.indexOf("ROTATE_LEFT") != -1) {
     commandAndValue.command = "ROTATE_LEFT";
     commandAndValue.value = getIntCommandValue(getCommand(incomingRequest, "ROTATE_LEFT"));
     return commandAndValue;
   } else if (incomingRequest.indexOf("CAMERA_CAPTURE") != -1) {
     commandAndValue.command = "CAMERA_CAPTURE";
     commandAndValue.value = 0;
     return commandAndValue;
   }
 }
 return commandAndValue;
}

void sendResponse(String msg) {
  String responseheader = "HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\nContent-Type: text/plain\r\nConnection: close\r\nContent-Length: 2\r\n\n"+msg+"\r\n";
  int len = responseheader.length();
  String openChannel = "AT+CIPSEND=0,"+String(len)+"\r\n";
  sendCommand(openChannel, 2000);
  delay(3000);
  softwareSerial.print(responseheader);
  delay(3000);
  sendCommand("AT+CIPCLOSE=0\r\n", 1000);
}

/////////////////////////////////////////
//          ArduCAM Code               //
/////////////////////////////////////////

void camera_configure() {
  uint8_t vid, pid;
  uint8_t temp;

#if defined(__SAM3X8E__)
  Wire1.begin();
#else
  Wire.begin();
#endif
//
  // Set CS pin as output
  pinMode(CS_PIN, OUTPUT);
  digitalWrite(CS_PIN, HIGH);

  // initialize SPI:
  SPI.begin();
  //
  // //Reset the CPLD
  // ARDU_CAMERA.write_reg(0x07, 0x80);
  // delay(100);
  // ARDU_CAMERA.write_reg(0x07, 0x00);
  // delay(100);
  //
  // while (1) {
  //   //Check if the ArduCAM SPI bus is OK
  //   ARDU_CAMERA.write_reg(ARDUCHIP_TEST1, 0x55);
  //   temp = ARDU_CAMERA.read_reg(ARDUCHIP_TEST1);
  //   if (temp != 0x55) {
  //     Serial.println(F("ACK CMD SPI interface Error!END"));
  //     delay(1000); continue;
  //   } else {
  //     Serial.println(F("ACK CMD SPI interface OK.END")); break;
  //   }
  // }
  //
  // while (1) {
  //   //Check if the camera module type is OV2640
  //   ARDU_CAMERA.wrSensorReg8_8(0xff, 0x01);
  //   ARDU_CAMERA.rdSensorReg8_8(OV2640_CHIPID_HIGH, &vid);
  //   ARDU_CAMERA.rdSensorReg8_8(OV2640_CHIPID_LOW, &pid);
  //   if ((vid != 0x26 ) && (( pid != 0x41 ) || ( pid != 0x42 ))) {
  //     Serial.println(F("ACK CMD Can't find OV2640 module!"));
  //     delay(1000); continue;
  //   }
  //   else {
  //     Serial.println(F("ACK CMD OV2640 detected.END")); break;
  //   }
  // }
  //
  // ARDU_CAMERA.set_format(JPEG);
  // ARDU_CAMERA.InitCAM();
  // ARDU_CAMERA.OV2640_set_JPEG_size(OV2640_320x240);
  // delay(1000);
  // ARDU_CAMERA.clear_fifo_flag();
  // delay(1000);

  Serial.println("CAMERA READY");
}
